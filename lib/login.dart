import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'posts_example.dart';
import 'dart:convert';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Login();
  }
}

class _Login extends State<Login> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  fLogin(context) async {
    String sToken = "";
    SharedPreferences _oSharePrefs = await SharedPreferences.getInstance();
    http.Response response =
        await http.post(Uri.parse('http://10.0.2.2:3000/login'),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              'username': usernameController.text,
              'password': passwordController.text,
            }));
    if (response.statusCode == 200) {
      sToken = jsonDecode(response.body)['token'];
      await _oSharePrefs.setString('token', sToken);
      Get.to(() => const PostsExample());
    } else {
      Get.showSnackbar(
        const GetSnackBar(
          message: 'Username and Password Invalid',
          icon: Icon(Icons.password),
          duration: Duration(seconds: 3),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    isLoggedIn();
  }

  //todo
  isLoggedIn() async {
    SharedPreferences _oSharePrefs = await SharedPreferences.getInstance();
    String? value = _oSharePrefs.getString("token");
    if (value != null) {
      Get.to(() => const PostsExample());
    } else {
      Get.to(() => const Login());
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Login Page'),
              backgroundColor: Colors.lightBlue,
            ), //Builder para trabajar con el contexto
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              child: Column(
                children: [
                  const FlutterLogo(size: 100),
                  TextField(
                      controller: usernameController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: "Email",
                      )),
                  TextField(
                      controller: passwordController,
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(), labelText: "Password")),
                  ElevatedButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                              const EdgeInsets.fromLTRB(60, 0, 60, 0)),
                          backgroundColor:
                              const MaterialStatePropertyAll<Color>(
                                  Colors.lightBlue),
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: const BorderSide(
                                          color: Colors.lightBlue)))),
                      onPressed: () {
                        fLogin(context);
                      },
                      child: const Text(
                        'Login',
                        style: TextStyle(fontSize: 20),
                      ))
                ],
              ),
            )));
  }
}
