import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'login.dart';

void main() {
  runApp(const GetMaterialApp(
    title: 'Login',
    home: Login(),
  ));
}
