import 'package:flutter/material.dart';

class PostsExample extends StatelessWidget {
  const PostsExample({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Posts'),
        backgroundColor: Colors.amber,
      ),
      body: const Center(
        child: Text('Your Posts Are Here!'),
      ),
    );
  }
}
